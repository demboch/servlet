package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import servlet.Candidate;

public class DataBaseCandidate {

	private final static String URL = "jdbc:mysql://localhost:3306/servlet"; // /form
	private final static String USER = "root";
	private final static String PASSWORD = "root";
	private final static String DRIVER = "com.mysql.jdbc.Driver";

	private Connection connection;
	private Statement statement;
	private SQLCandidateQuery sqlCandidateQuery;

	public DataBaseCandidate() {
		sqlCandidateQuery = new SQLCandidateQuery();
	}

	private void runUpdate(String query) throws ClassNotFoundException, SQLException {
		Class.forName(DRIVER);
		connection = DriverManager.getConnection(URL, USER, PASSWORD);
		statement = connection.createStatement();
		statement.executeUpdate(query);
	}

	public void save(Candidate candidate) {

		String query = sqlCandidateQuery.createSaveQuery(candidate);

		try {
			runUpdate(query);
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private int runCountQuery(String query) {
		int count = 0;

		try {
			Class.forName(DRIVER);
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);

			while (result.next()) {
				count = result.getInt(1);
			}

		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return count;
	}

	public int countCandidate() {
		String query = sqlCandidateQuery.createGetCountQuery();
		return runCountQuery(query);
	}

	public int countByEmail(String email) {
		String query = sqlCandidateQuery.createGetCountByEmailQuery(email);
		return runCountQuery(query);
	}

}
