package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.DataBaseCandidate;

@WebServlet("/GetCandidate")
public class GetCandidate extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private SetCandidate candidateChecker;
	private DataBaseCandidate candidateDB;
	private HttpSession session;
	private String message;

	public GetCandidate() {
		candidateChecker = new SetCandidate();
		candidateDB = new DataBaseCandidate();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (candidateDB.countCandidate() >= 5) {
			message = "Brak wolnych miejsc!";
		}
		request.setAttribute("count", candidateDB.countCandidate());
		request.setAttribute("message", message);
		request.getRequestDispatcher("CandidateRegisterForm.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (candidateDB.countCandidate() < 5) {
			Candidate candidate = candidateChecker.checkCandidateForm(request);
			if (candidate.getEmail().equals(candidate.getConfirmEmail())) {
				if (candidateDB.countByEmail(candidate.getEmail()) == 0) {
					session = request.getSession(true);
					candidateDB.save(candidate);
					message = "Zapisano kandydata.";
				} else {
					response.sendRedirect("IfCandidateExist.jsp");
					return;
				}
			} else
				message = "Niepoprawny E-mail!";
		} else
			message = "Brak wolnych miejsc!";

		doGet(request, response);
	}

}
