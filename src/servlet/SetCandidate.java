package servlet;

import javax.servlet.http.HttpServletRequest;

public class SetCandidate {

	public Candidate checkCandidateForm(HttpServletRequest request) {

		Candidate candidate = new Candidate();

		try {
			String selectedCheckbox = "";
			for (String checkbox : request.getParameterValues("checkbox")) {
				selectedCheckbox += checkbox + ", ";
			}
			candidate.setCheckbox(selectedCheckbox);
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		candidate.setFirstName(request.getParameter("firstName"));
		candidate.setSurname(request.getParameter("surname"));
		candidate.setEmail(request.getParameter("email"));
		candidate.setConfirmEmail(request.getParameter("confirmEmail"));
		candidate.setEmployer(request.getParameter("employer"));
		candidate.setOther(request.getParameter("other"));
		candidate.setHobby(request.getParameter("hobby"));

		return candidate;
	}

}
