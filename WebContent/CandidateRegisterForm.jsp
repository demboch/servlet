<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>FORMULARZ</title>
	</head>
<body bgcolor="black" text="red">

	<h1 align="center">Formularz rejestracyjny na konferencje "Java 4 US!"</h1>

	<form action="http://localhost:8090/ServletRegisterForm/GetCandidate" method="POST">

		<p>Liczba zapisanych kandydatów: ${count}</p>
		<p>${message}</p>

		<hr>

		<table>
			<tr>
				<td>Imie: <input type="text" name="firstName" required></td>
			</tr>

			<tr>
				<td>Nazwisko: <input type="text" name="surname" required></td>
			</tr>

			<tr>
				<td>E-mail: <input type="text" name="email" required></td>
			</tr>
			<tr>
				<td>Potwierdz e-mail: <input type="text" name="confirmEmail" required></td>
			</tr>
			<tr>
				<td>Nazwa pracodawcy: <input type="text" name="employer"></td>
			<tr>
				<td>Skad dowiedziales sie o konferencji?</td>
			</tr>
			<tr>
				<td>
					<input type="checkbox" name="checkbox" value="Ad at work"> Ogloszenie w pracy<br />
					<input type="checkbox" name="checkbox" value="Ad university"> Ogloszenie na uczelni<br />
					<input type="checkbox" name="checkbox" value="Facebook"> Facebook<br />
					<input type="checkbox" name="checkbox" value="Friends"> Znajomi<br />
				</td>
			</tr>
			<tr>
				<td>Inne?</td>
			</tr>
			<tr>
				<td><textarea name="other" rows="2" cols="20"></textarea></td>
			</tr>
			<tr>
				<td>Czym sie zajmujesz?</td>
			</tr>
			<tr>
				<td><textarea name="hobby" rows="2" cols="20"></textarea></td>
			</tr>

		</table>
		<input type="submit" value="Wyslij">
	</form>


</body>
</html>



